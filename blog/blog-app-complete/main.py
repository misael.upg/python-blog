import os
import webapp2
import jinja2
from google.appengine.ext import ndb
import datetime
import hashlib
import random
import string
import re
import logging
import json
from google.appengine.api import memcache

## Set directory to fetch html templates
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

# regex for validation
USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return USER_RE.match(username)

PASSWORD_RE = re.compile("^.{3,20}$")
def valid_password(password):
    return PASSWORD_RE.match(password)

EMAIL_RE = re.compile("^[\S]+@[\S]+.[\S]+$")
def valid_email(email):
    return EMAIL_RE.match(email)

def userExists(username):
    user = User.get_user(username)
    if user:
        return True
    else:
        return False

def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))

def make_pw_hash(name, pw, salt=make_salt()):
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (h, salt)

def valid_pw(name, pw, h):
    return make_pw_hash(name, pw, h.split(",")[1]) == h

def hash_str(s):
    return hashlib.md5(s).hexdigest()

def make_secure_val(s):
    return "%s|%s" % (s, hash_str(s))

def check_secure_val(h):
    if h.split('|')[1] == hash_str(h.split('|')[0]):
        return h.split('|')[0]
    else:
        return None



## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Base Handler
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render (params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    # validatin methods
    def validate_user_data(self, username, email, password, verify):
        user = User.get_user(username)
        errors = {}
        if valid_password(password):
            if password != verify:
                errors['verifyError'] = "Your passwords doesn't match"
            else:
                errors['verifyError'] = " "
            errors['passwordError'] = " "
        else:
            if password != verify:
                errors['verifyError'] = " "
            errors['passwordError'] = "Enter a valid password"
            errors['verifyError'] = " "
        if not valid_username(username):
            errors['userNameError'] = "Enter a valid username"
        else:
            if userExists(username):
                errors['userNameError'] = "This user name already exists"
            else:
                errors['userNameError'] = " "
        if email and not valid_email(email):
            errors['emailError'] = "Enter a valid email"
        else:
            errors['emailError'] = " "
        #self.response.write(errors)
        self.renderForm(
            errors['userNameError'],
            errors['passwordError'],
            errors['verifyError'],
            errors['emailError'],
            username = username,
            email = email
            );

    def set_secure_cookie(self, user_id):
        self.response.headers.add_header('Set-Cookie', 'user=%s; Path=/' % make_secure_val(str(user_id)))

    def delete_cookie(self):
        self.response.headers.add_header('Set-Cookie', 'user=; Path=/')

    def getFormat(self):
        if self.request.url.endswith('.json'):
            return 'json'
        else:
            return 'html'

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## WelcomePage
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class WelcomePage(Handler):
    def get(self):
        user = self.request.cookies.get('user')
        if user and check_secure_val(user):
            user_id = user.split('|')[0]
            username = User.get_by_id(int(user_id)).userName
            self.response.write('<h1>Welcome, ' + username + '</h1>')
        else:
            self.redirect('/signup')

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Signup
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class SignUpPage(Handler):
    def save_user(self, username, password, email):
        return User.put_user(username, password, email)

    def renderForm(
            self,
            userNameError = "",
            passwordError = "",
            verifyError   = "",
            emailError    = "",
            username = "",
            email = "" ):
        self.render(
            'signup_form.html',
            userNameError = userNameError,
            passwordError = passwordError,
            verifyError   = verifyError,
            emailError    = emailError,
            username = username,
            email = email )

    def get(self):
        self.renderForm();

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify   = self.request.get('verify')
        email    = self.request.get('email')

        if (username and password and verify):
            if valid_username(username) and valid_password(password) and password == verify and (email == "" or valid_email(email)) and not userExists(username):
                user = self.save_user(username, password, email)
                user_id = user.id()
                self.set_secure_cookie(user_id)
                self.redirect('/welcome')
            else:
                self.validate_user_data(username, email, password, verify)
        else:
            self.validate_user_data(username, email, password, verify)

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Login
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class LoginPage(Handler):
    def get(self):
        self.render('login_form.html', loginError = "")

    def post(self):
        username = self.request.get('username')
        userPassword = self.request.get('password')

        if username and userPassword:
            #Get user fomr DB if existent
            user = User.get_user(username)

            if user:
                user_id = user.key.id()
                ## Check if password match the one in DB
                if valid_pw(username, userPassword, user.userPassword):
                    self.set_secure_cookie(user_id)
                    self.redirect('/welcome')
                else:
                    self.render('login_form.html', loginError="Fuck you impostor")
            else:
                self.render('login_form.html', loginError="Could not find user in DB")
        else:
            self.render('login_form.html', loginError="Invalid Login")

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Logout
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class LogoutPage(Handler):
    def get(self):
        self.delete_cookie()
        self.redirect('/signup')

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## New Post
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class NewPostHandler(Handler):
    def save_post(self, post):
        self.post_key = Post.put_post(post)
        memcache.add('post_%s' % self.post_key.id(), post, 100000)
        posts_keys = memcache.get('posts_keys')
        posts_keys.append(str(self.post_key.id()))
        memcache.set('posts_keys', posts_keys)
        queried_post_time = datetime.datetime.now()
        memcache.add('queried_post_time_%s' % self.post_key.id(), queried_post_time, 100000)
        return self.post_key

    def get(self):
        user = self.request.cookies.get('user')
        if user and check_secure_val(user):
            self.render('new_post.html')
        else:
            self.redirect('/signup')

    def post(self):
        title = self.request.get('title')
        content = self.request.get('content')
        error = ""
        user = self.request.cookies.get('user')
        if user and check_secure_val(user):
            if not (title and content):
                error = "You must complete both fields"
                self.render('new_post.html', error=error, title=title, content=content)
            else:
                post = Post( title = title, content = content)
                post_key = self.save_post(post)
                self.redirect('/' + str(post_key.id()))
        else:
            self.write('You are not autorized to send this information')

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## HomePage
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class HomeHandler(Handler):
    def render_home(self, posts=[], queried_front = ""):
        self.render('front_page.html', posts = posts, queried_front = queried_front)

    def get_posts(self, posts_per_page = 20):
        posts_keys = memcache.get('posts_keys')
        if posts_keys is not None:
            posts = memcache.get_multi(posts_keys, key_prefix="post_")
            return posts
        else:
            posts = dict((str(post.key.id()), post) for post in Post.query_posts(posts_per_page))
            logging.error('DB Query')
            queried_time = datetime.datetime.now()
            posts_keys = posts.keys()
            queried_dates = dict(('queried_post_time_%s' % key, queried_time) for key in posts_keys)
            memcache.add_multi(posts, key_prefix="post_", time = 100000)
            memcache.add_multi(queried_dates, time = 100000)
            memcache.add('posts_keys', posts_keys, 100000)
            memcache.add('queried_front', queried_time, 100000)
            return posts

    def get(self, format):
        error = ""
        posts = self.get_posts()
        time_difference = datetime.datetime.now() - memcache.get('queried_front')
        queried_front = "Queried %s seconds ago" % time_difference.seconds
        if self.getFormat() == 'json':
            posts = [{"title": str(post.title), "content": str(post.content), "date": str(post.date.strftime("%b %d, %Y"))} for post in posts]
            posts_json = json.dumps(posts)
            self.response.headers['Content-Type'] = "application/json"
            self.response.out.write(posts_json)
        else:
            self.render_home( posts = posts, queried_front = queried_front )
## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Post Permalink
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------

class PostHandler(Handler):
    def get_post(self, post_id):
        post = memcache.get('post_%s' % post_id)
        if post is not None:
            return post
        else:
            post = Post.get_by_id(int(post_id))
            logging.error('DB Query')
            memcache.add('post_%s' % post_id, post, 100000)
            queried_post_time = datetime.datetime.now()
            memcache.add('queried_post_time_%s' % post_id, queried_post_time, 100000)
            return post

    def get(self, post_id):
        post_id = post_id.split('.')[0]
        post = self.get_post(post_id)
        time_difference = datetime.datetime.now() - memcache.get('queried_post_time_%s' % post_id)
        queried_post = "Queried %s seconds ago" % time_difference.seconds
        if self.getFormat() == 'json':
            post = {"title": post.title, "content": post.content}
            post_json = json.dumps(post)
            self.response.headers['Content-Type'] = "application/json"
            self.response.out.write(post_json)
        else:
            self.render('post_viewer.html', post = post, queried_post = queried_post)

## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Flush Memcache
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
class FlushHandler(Handler):
    def get(self):
        memcache.flush_all() 
 
## ------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------
## Models
##------------------------------------------------------------------------------------------------------------
## ------------------------------------------------------------------------------------------------------------

class User(ndb.Model):
    userName = ndb.StringProperty(required=True)
    userEmail = ndb.StringProperty(required=False)
    userPassword = ndb.TextProperty(required=True)
    signupDate = ndb.DateTimeProperty(auto_now_add = True)

    @classmethod
    def get_user(cls, username):
        #returns user fomr DB if existent, None otherwise
        return cls.query(User.userName == username).get()

    @classmethod
    def put_user(cls, username, password, email):
        hash_password = make_pw_hash(username, password)
        user = User(
            userName     = username,
            userPassword = hash_password,
            userEmail    = email
        )
        user_key = user.put()
        return user_key


class Post(ndb.Model):
    title = ndb.StringProperty(required= True)
    content = ndb.TextProperty(required = True)
    date = ndb.DateTimeProperty(auto_now_add = True)

    @classmethod
    def query_posts(cls, post_per_page):
        return cls.query().order(-cls.date).fetch(post_per_page)

    @classmethod
    def put_post(cls, post):
        return post.put()


## Routes
app = webapp2.WSGIApplication([
    webapp2.Route('/<format:(?:\.json)?>', handler=HomeHandler, name='home'),
    webapp2.Route('/newpost', handler=NewPostHandler, name='new-post'),
    webapp2.Route('/<post_id:([0-9]+)(?:\.json)?>', handler=PostHandler, name='post'),
    webapp2.Route('/signup', handler=SignUpPage, name='signup'),
    webapp2.Route('/welcome', handler=WelcomePage, name='welcome'),
    webapp2.Route('/login', handler=LoginPage, name='login'),
    webapp2.Route('/logout', handler=LogoutPage, name='logout'),
    webapp2.Route('/flush', handler=FlushHandler, name='flush')
], debug = True)
