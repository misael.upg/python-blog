import os
import webapp2
import jinja2
from google.appengine.ext import ndb
import datetime

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

## Main Handler
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render (params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


class HomeHandler(Handler):
    POST_PER_PAGE = 20

    def render_home(self, posts=[]):
        self.render('front_page.html', posts=posts)

    def get(self):
        error = ""
        posts = Post.query_posts().fetch(self.POST_PER_PAGE)
        self.render_home(posts=posts)


class NewPostHandler(Handler):

    def get(self):
        self.render('new_post.html')

    def post(self):
        title = self.request.get('title')
        content = self.request.get('content')
        error = ""

        if not (title and content):
            error = "You must complete both fields"
            self.render('new_post.html', error=error, title=title, content=content)
        else:
            post = Post(
                title   = title,
                content = content)

            post_key = post.put()

            self.redirect('/' + str(post_key.id()))




class PostHandler(Handler):
    def get(self, post_id):
        post = Post.get_by_id(int(post_id))
        self.render('post.html', post=post)





##Models
class Post(ndb.Model):
    title = ndb.StringProperty(required= True)
    content = ndb.TextProperty(required = True)
    date = ndb.DateTimeProperty(auto_now_add = True)

    @classmethod
    def query_posts(cls):
        return cls.query().order(-cls.date)


app = webapp2.WSGIApplication([
    webapp2.Route('/', handler=HomeHandler, name='home'),
    webapp2.Route('/new_post', handler=NewPostHandler, name='new-post'),
    webapp2.Route('/<post_id>', handler=PostHandler, name='post'),
], debug = True)
