import webapp2
import cgi
import codecs
import re

## Form Template

form = """
<form method="post">
    <label for="text">

        <h1>Enter some text to ROT13</h1>
        <textarea name="text" cols="30" rows="10">%(text)s</textarea>
        <br>
        <input type="submit">
    </label>
</form>
""" 


## Escape HTML
def escape_html(s):
    return cgi.escape(s, quote = True)

signUpForm = """ 
    <h1>Sign Up</h1>
    <form method="post">
        <label for="username">Username
            <input type="text" name="username" id="username" value="%(username)s">
        </label><h2 style="color:red; display:inline-block">%(userNameError)s</h2><br>
        <label for="password">Password
            <input type="text" name="password" id="password">
        </label><h2 style="color:red; display:inline-block">%(passwordError)s</h2><br>
        <label for="verify">Confirm your password
            <input type="text" name="verify" id="verify">
        </label><h2 style="color:red; display:inline-block">%(verifyError)s</h2><br>
        <label for="email">Email
            <input type="text" name="email" id="email" value="%(email)s">
        </label><h2 style="color:red; display:inline-block">%(emailError)s</h2><br>
        <input type="submit" value="Submit">
    </form> 
"""

USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return USER_RE.match(username)

PASSWORD_RE = re.compile("^.{3,20}$")
def valid_password(password):
    return PASSWORD_RE.match(password)

EMAIL_RE = re.compile("^[\S]+@[\S]+.[\S]+$")
def valid_email(email):
    return EMAIL_RE.match(email)


## Handlers
class rot13Page(webapp2.RequestHandler):
    def write_form(self, text):
        self.response.write(form % {"text": text})

    def rot13(self, string):
        chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz "
        trans = chars[27:]+chars[:27]
        rot_char = lambda c: trans[chars.find(c)] if chars.find(c) > -1 else c
        return ''.join(rot_char(c) for c in string)

    def get(self):
        self.write_form(text="")

    def post(self):

        originalText = self.request.get('text')
        rotText = self.rot13(originalText);
        self.write_form(escape_html(rotText))

class signUpPage(webapp2.RequestHandler):
    def writeSignUpForm(self, username="", email="", errors = {
            'userNameError': " ", 
            'emailError'   : " ", 
            'verifyError'  : " ", 
            'passwordError': " "
            }):
        self.response.write(errors)
        self.response.write(signUpForm % {
            "username"     : username, 
            "email"        : email,
            "userNameError": errors['userNameError'],
            "emailError"   : errors['emailError'],
            "passwordError": errors['passwordError'],
            "verifyError"  : errors['verifyError']
            })

    def validate_user_data(self, username, email, password, verify):
        errors = {}
        if valid_password(password):
            if password != verify:
                errors['verifyError'] = "Your passwords doesn't match"
            else:
                errors['verifyError'] = " "
            errors['passwordError'] = " "
        else: 
            if password != verify:
                errors['verifyError'] = " "
            errors['passwordError'] = "Enter a valid password"
            errors['verifyError'] = " "
        if not valid_username(username):
            errors['userNameError'] = "Enter a valid username"
        else:
            errors['userNameError'] = " "
        if email and not valid_email(email):
            errors['emailError'] = "Enter a valid email"        else:
            errors['emailError'] = " "
        #self.response.write(errors)
        self.writeSignUpForm(username, email, errors)

    def get(self):
        self.writeSignUpForm()

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify   = self.request.get('verify')
        email    = self.request.get('email')


        if (username and password and verify):
            if valid_username(username) and valid_password(password) and password == verify and (email == "" or valid_email(email)): 
                self.redirect('/welcome'+'?username=' + self.request.get('username'))
            else:
                self.validate_user_data(username, email, password, verify)
        else:
            self.validate_user_data(username, email, password, verify)





class welcomePage(webapp2.RequestHandler):
    def get(self):
        self.response.write('<h1>Welcome, ' + self.request.get('username') + '</h1>')
    


app = webapp2.WSGIApplication([
    ('/rot13', rot13Page), ('/signup', signUpPage), ('/welcome', welcomePage)
], debug=True)
