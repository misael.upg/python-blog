import os
import webapp2
import jinja2

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

## Main Handler
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render (params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))


## Main page handler, inherited from Handler
class MainPage(Handler):
    ## To render template with data provided
    def render_front(self, title="", art="", error=""):
        self.render("front.html", title=title, art=art, error=error)
    ## Render form for the first time
    def get(self):
        self.render_front()
    ## Submit information
    def post(self):
        title = self.request.get("title")
        art = self.request.get("art")

        ## Validate fields
        if title and art:
            self.redirect("/thanks")
        else:
            error = "you must enter a title and an art"
            self.render_front(title, art, error)

## If succesful submitted, redirect to htanks page
class ThanksHandler(Handler):
    def get(self):
        self.write("Thanks")
        
        

app = webapp2.WSGIApplication([
    ('/', MainPage), ('/thanks', ThanksHandler)
], debug=True)

