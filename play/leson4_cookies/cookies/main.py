#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import webapp2
import jinja2
import re
from google.appengine.ext import ndb
import datetime
import hashlib
import random
import string

# Set the configuration needed for jinja templates to work
template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

# regex for validation
USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return USER_RE.match(username)

PASSWORD_RE = re.compile("^.{3,20}$")
def valid_password(password):
    return PASSWORD_RE.match(password)

EMAIL_RE = re.compile("^[\S]+@[\S]+.[\S]+$")
def valid_email(email):
    return EMAIL_RE.match(email)

def userExists(username):
    user = User.get_user(username)
    if user:
        return True
    else:
        return False

def make_salt():
    return ''.join(random.choice(string.letters) for x in xrange(5))

def make_pw_hash(name, pw, salt=make_salt()):
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (h, salt)

def valid_pw(name, pw, h):
    return make_pw_hash(name, pw, h.split(",")[1]) == h

def hash_str(s):
    return hashlib.md5(s).hexdigest()

def make_secure_val(s):
    return "%s|%s" % (s, hash_str(s))

def check_secure_val(h):
    if h.split('|')[1] == hash_str(h.split('|')[0]):
        return h.split('|')[0]
    else:
        return None

# Base Handler
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render (params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    # validatin methods
    def validate_user_data(self, username, email, password, verify):
        user = User.get_user(username)
        errors = {}
        if valid_password(password):
            if password != verify:
                errors['verifyError'] = "Your passwords doesn't match"
            else:
                errors['verifyError'] = " "
            errors['passwordError'] = " "
        else:
            if password != verify:
                errors['verifyError'] = " "
            errors['passwordError'] = "Enter a valid password"
            errors['verifyError'] = " "
        if not valid_username(username):
            errors['userNameError'] = "Enter a valid username"
        else:
            if userExists(username):
                errors['userNameError'] = "This user name already exists"
            else:
                errors['userNameError'] = " "
        if email and not valid_email(email):
            errors['emailError'] = "Enter a valid email"
        else:
            errors['emailError'] = " "
        #self.response.write(errors)
        self.renderForm(
            errors['userNameError'],
            errors['passwordError'],
            errors['verifyError'],
            errors['emailError'],
            username = username,
            email = email
            );

    def set_secure_cookie(self, user_id):
        self.response.headers.add_header('Set-Cookie', 'user=%s; Path=/' % make_secure_val(str(user_id)))

    def delete_cookie(self):
        self.response.headers.add_header('Set-Cookie', 'user=; Path=/')

# SignUpPage
class SignUpPage(Handler):
    def save_user(self, username, password, email):
        return User.put_user(username, password, email)


    def renderForm(
            self,
            userNameError = "",
            passwordError = "",
            verifyError   = "",
            emailError    = "",
            username = "",
            email = "" ):
        self.render(
            'form.html',
            userNameError = userNameError,
            passwordError = passwordError,
            verifyError   = verifyError,
            emailError    = emailError,
            username = username,
            email = email )

    def get(self):
        self.renderForm();

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        verify   = self.request.get('verify')
        email    = self.request.get('email')

        if (username and password and verify):
            if valid_username(username) and valid_password(password) and password == verify and (email == "" or valid_email(email)) and not userExists(username):
                user = self.save_user(username, password, email)
                user_id = user.id()
                self.set_secure_cookie(user_id)
                self.redirect('/welcome')
            else:
                self.validate_user_data(username, email, password, verify)
        else:
            self.validate_user_data(username, email, password, verify)

class LoginPage(Handler):
    def get(self):
        self.render('login_form.html', loginError = "")

    def post(self):
        username = self.request.get('username')
        userPassword = self.request.get('password')

        if username and userPassword:
            #Get user fomr DB if existent
            user = User.get_user(username)

            if user:
                user_id = user.key.id()
                ## Check if password match the one in DB
                if valid_pw(username, userPassword, user.userPassword):
                    self.set_secure_cookie(user_id)
                    self.redirect('/welcome')
                else:
                    self.render('login_form.html', loginError="Fuck you impostor")
            else:
                self.render('login_form.html', loginError="Could not find user in DB")
        else:
            self.render('login_form.html', loginError="Invalid Login")


class WelcomePage(Handler):
    def get(self):
        user = self.request.cookies.get('user')
        if user and check_secure_val(user):
            user_id = user.split('|')[0]
            username = User.get_by_id(int(user_id)).userName
            self.response.write('<h1>Welcome, ' + username + '</h1>')
        else:
            self.redirect('/signup')

class LogoutPage(Handler):
    def get(self):
        self.delete_cookie()
        self.redirect('/signup')


# Model
class User(ndb.Model):
    userName = ndb.StringProperty(required=True)
    userEmail = ndb.StringProperty(required=False)
    userPassword = ndb.TextProperty(required=True)
    signupDate = ndb.DateTimeProperty(auto_now_add = True)

    @classmethod
    def get_user(cls, username):
        #returns user fomr DB if existent, None otherwise
        return cls.query(User.userName == username).get()

    @classmethod
    def put_user(cls, username, password, email):
        hash_password = make_pw_hash(username, password)
        user = User(
            userName     = username,
            userPassword = hash_password,
            userEmail    = email
        )
        user_key = user.put()
        return user_key




app = webapp2.WSGIApplication([
    webapp2.Route('/signup', handler=SignUpPage, name='signup'),
    webapp2.Route('/welcome', handler=WelcomePage, name='welcome'),
    webapp2.Route('/login', handler=LoginPage, name='login'),
    webapp2.Route('/logout', handler=LogoutPage, name='logout')
], debug = True)
