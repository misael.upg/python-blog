import webapp2
import cgi

form = """
<form method="post">
    What is your birthday?
    <br>
    <label for="month">Month</label>
    <input type="text" name="month" value="%(month)s">
    <label for="day">Day</label>
    <input type="text" name="day" value="%(day)s"> 
    <label for="year">Year</label>
    <input type="text" name="year" value="%(year)s">
    <span style="color:red">%(error)s</span>
    <br>
    <br>
    <input type="submit">
</form>
"""

def escape_html(s):
    return cgi.escape(s, quote = True)

def valid_year(year):
    if year and year.isdigit():
        year = int(year)
        if year >= 1900 and year <=2020:
            return year

def valid_day(day):
    if day.isdigit() and int(day) <=31 and int(day) >=1:
        return int(day)
    return None

months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']

month_abbvs = dict((m[:3].lower(),m) for m in months)
          
def valid_month(month):
    if month:
        short_month = month[:3].lower()
        return month_abbvs.get(short_month)


class MainHandler(webapp2.RequestHandler):

    def write_form(self, error="", month="", day="", year=""):
        self.response.out.write(form % {
            "error": error, 
            "month": escape_html(month), 
            "day"  : escape_html(day,),
            "year" : escape_html(year) 
            })

    def get(self):
        #self.response.headers['Content-Type'] = 'text/html'
        self.write_form()

    def post(self):

        user_month = self.request.get('month')
        user_day   = self.request.get('day')
        user_year  = self.request.get('year')

        month = valid_month(user_month)
        day = valid_day(user_day)
        year = valid_year(user_year)

        if not (month and day and year):
            self.write_form("That doesn't look valid to me", user_month, user_day, user_year)

        else: 
            self.redirect('/thanks')

class ThanksHandler(webapp2.RequestHandler):
    def get(self):
        self.response.out.write("Hey, thanks for submitting your data")
        

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/thanks', ThanksHandler)
], debug=True)

